<?php
/**
 * Available variables:
 *  $nid
 *  $zebra
 *  $id
 *  $directory
 *  $is_admin
 *  $is_front
 *  $logged_in
 *  $db_is_active
 *  $user
 *  $username
 *  $client_name
 *  $client_path
 *  $key
 *  $update_username
 *  $update_realname
 *  $site
 *  $uri
 *  $uri_brief
 *  $uri_login
 *  $mailto
 *  $date
 *  $ticket_subject
 *  $ticket_body
 *  $ticket_url
 *  $update_url
 *  $update
 *  $state
 *  $priority
 *  $assigned_username
 *  $assigned_realname
 *  $unsubscribe_ticket
 *  $unsubscribe_all
 */
?>
<p>
Thanks for contacting us. A support representative will reply as soon as possible.
</p>
<p>
You may <a href="<?php print $ticket_url;?>">click here</a> to view your ticket.
<p>