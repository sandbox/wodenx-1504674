<?php
/**
 * Define this Export UI plugin.
 */
$plugin = array(
  'schema' => 'support_client',  // As defined in hook_schema().
  'access' => 'administer support',  // Define a permission users must have to access these pages.

  // Define the menu item.
  'menu' => array(
    'menu prefix' => 'admin/support',
    'menu item' => 'support_tweaks',
    'menu title' => 'Support Clients',
    'menu description' => 'Administer Support clients.',
  ),

  // Define user interface texts.
  'title singular' => t('client'),
  'title plural' => t('clients'),
  'title singular proper' => t('Support client'),
  'title plural proper' => t('Support clients'),

  // Define the names of the functions that provide the add/edit forms.
  'form' => array(
    'settings' => 'support_ctools_export_ui_form',
    //'validate' => 'support_ctools_export_ui_valiate',
    //'submit' => 'support_ctools_export_ui_submit',
  ),
);

function support_ctools_export_ui_form(&$form, &$form_state) {
  module_load_include('inc', 'support', 'support.admin');
  $form =  support_admin_client($form_state, $form_state['item']);
}

//function support_ctools_export_ui_validate($form, &$form_state) {
  
//}

//function support_ctools_export_ui_submit($form, &$form_state) {
  
//}
